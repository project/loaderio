# LoaderIO

This module provides ease in doing load/stress testing.

LoaderIO module provides Drupal users a way to test their website for performance.
This module, having an insight into your Drupal installation, generates an insight about your site using Loader.IO API

## Usage

Register at loader.io and grab your api key
Create a test instance in loader dashboard, fill in the required field including your api key


