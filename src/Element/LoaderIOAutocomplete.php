<?php
/**
 * @file
 * LoaderIO Test autocomplete element.
 */

namespace Drupal\loaderio\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\Element\EntityAutocomplete;
use Drupal\loaderio\Entity\LoaderIO;

/**
 * Provides a "loaderio_autocomplete" element.
 *
 * @RenderElement("loaderio_autocomplete")
 */
class LoaderIOAutocomplete extends EntityAutocomplete {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $info = parent::getInfo();

    $info['#target_type'] = $info['#entity_type'] = LoaderIO::ENTITY_TYPE;

    return $info;
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    if (is_string($element['#default_value'])) {
      $element['#default_value'] = LoaderIO::load($element['#default_value']);
    }
    elseif (is_array($element['#default_value']) && !empty(array_filter($element['#default_value'], 'is_string'))) {
      $element['#default_value'] = LoaderIO::loadMultiple($element['#default_value']);
    }

    return parent::valueCallback($element, $input, $form_state);
  }

}
