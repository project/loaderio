<?php
/**
 * @file
 * LoaderIO Test entity default form.
 */

namespace Drupal\loaderio\Entity\Form\LoaderIO;

// Entity form.
use Drupal\Core\Entity\EntityForm;
// Form state.
use Drupal\Core\Form\FormStateInterface;
// Entity object.
use Drupal\loaderio\Entity\LoaderIO;
// Link.
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\GeneratedLink;

class DefaultForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    /** @var LoaderIO $entity */
    $entity = $this->getEntity();

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#required' => TRUE,
      '#default_value' => $entity->label,
    ];

    $form['name'] = [
      '#type' => 'machine_name',
      '#disabled' => !$entity->isNew(),
      '#default_value' => $entity->name,
      '#machine_name' => [
        'source' => ['label'],
        'exists' => [$entity, 'load'],
      ],
    ];

    $form['test_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Test Types'),
      '#multiple' => TRUE,
      '#default_value' => $entity->test_types,
      '#description' => $this->t('Test Types'),
      '#options' => [
        'total' => $this->t('Client per Test'),
        'persecond' => $this->t('Client requests made each second. '),
        'concurrent' => $this->t('Maintain client lode'),
      ],
    ];

    $form['key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API key'),
      '#default_value' => $entity->key,
      '#description' => $this->t('How to create an application with API key? Read here: @href', [
        '@href' => static::externalLink('http://docs.loader.io/api/intro.html'),
      ]),
    ];

    $form['initial'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Initial'),
      '#default_value' => $entity->initial,
      '#description' => $this->t('Number of connections to start with (ignored for non-cycling tests)'),
    ];

    $form['duration'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Duration'),
      '#default_value' => $entity->duration,
      '#description' => $this->t('Duration of test, in seconds?'),
    ];

    $form['total'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Total'),
      '#default_value' => $entity->total,
      '#description' => $this->t('Duration of test, in seconds?'),
    ];


    $form['callback'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Initialization callback'),
      '#description' => $this->t('The name of JS function which will be triggered when API will be completely loaded.'),
      '#default_value' => $entity->callback,
    ];

    return parent::form($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    /** @var LoaderIO $entity */
    $entity = $this->getEntity();

    drupal_set_message($this->t('The @entity_type %label has been @operation.', [
      '@entity_type' => $entity->getEntityType()->getLowercaseLabel(),
      '%label' => $entity->label,
      '@operation' => $entity->isNew() ? $this->t('created') : $this->t('updated'),
    ]));

    $form_state->setRedirectUrl($entity->toUrl('collection'));
  }

  /**
   * Create external link.
   *
   * @param string $href
   *   URL string.
   *
   * @return GeneratedLink
   *   Generated link object.
   */
  protected static function externalLink($href) {
    return Link::fromTextAndUrl($href, Url::fromUri($href, ['attributes' => ['target' => '_blank']]))->toString();
  }

}
