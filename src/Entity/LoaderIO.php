<?php
/**
 * @file
 * LoaderIO Test entity.
 */

namespace Drupal\loaderio\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Url;

/**
 * Defines the "testloaderio" configuration entity.
 *
 * @link https://www.drupal.org/node/2207559
 *
 * @ConfigEntityType(
 *   id = "testloaderio",
 *   label = @Translation("LoaderIO Test"),
 *   config_prefix = "testloaderio",
 *   admin_permission = "administer loaderio tests",
 *   handlers = {
 *     "list_builder" = "Drupal\loaderio\Entity\ListBuilder\LoaderIO\DefaultListBuilder",
 *     "form" = {
 *       "default" = "Drupal\loaderio\Entity\Form\LoaderIO\DefaultForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *   },
 *   entity_keys = {
 *     "id" = "name",
 *     "label" = "label",
 *   },
 *   links = {
 *     "collection" = "/admin/config/services/loaderio-tests",
 *     "edit-form" = "/admin/config/services/loaderio-test/{testloaderio}",
 *     "delete-form" = "/admin/config/services/loaderio-test/{testloaderio}/delete",
 *   },
 * )
 */
class LoaderIO extends ConfigEntityBase implements LoaderIOInterface {

  /**
   * Human-readable label of an entry.
   *
   * @var string
   */
  public $label = '';
  /**
   * Machine-readable name of an entry.
   *
   * @var string
   */
  public $name = '';
  /**
   * LoaderIO API key.
   *
   * @var string
   */
  public $key = '';
  /**
   * LoaderIO Test API Key.
   *
   * @var string[]
   */

   public $initial = '';
   /**
    * LoaderIO Test Inital.
    *
    * @var string[]
   */

   public $duration = '';
   /**
    * LoaderIO Test Duration.
    *
    * @var string[]
   */

   public $total = '';
   /**
    * LoaderIO Test Total.
    *
    * @var string[]
   */

  public $test_types = [];
  /**
   * Name of JS initialization callback.
   *
   * @var string
   */
  public $callback = '';

  /**
   * {@inheritdoc}
   */
  public function __construct(array $values) {
    parent::__construct($values, static::ENTITY_TYPE);
  }

  /**
   * {@inheritdoc}
   */
  public function save() {
    if (empty($this->label)) {
      throw new \InvalidArgumentException('The "label" property is required!');
    }

    if (empty($this->name)) {
      $this->name = preg_replace(['/[^a-z0-9_]+/', '/_+/'], '_', strtolower(
        \Drupal::service('transliteration')
          ->transliterate($this->label, LanguageInterface::LANGCODE_DEFAULT, '_'))
      );
    }

    return parent::save();
  }

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->{$this->getEntityType()->getKey('id')};
  }

  /**
   * {@inheritdoc}
   *
   * @return Url
   *   Url object.
   */
  public function toUrl($rel = 'canonical', array $options = []) {
    return parent::toUrl($rel, $options);
  }

}
