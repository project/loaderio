<?php
/**
 * @file
 * LoaderIO Test entity list builder.
 */

namespace Drupal\loaderio\Entity\ListBuilder\LoaderIO;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

class DefaultListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [];

    $header['label'] = $this->t('Label');
    $header['key'] = $this->t('API key');
    $header['test_types'] = $this->t('Test types');
    $header['initial'] = $this->t('Initial');
    $header['duration'] = $this->t('Duration');
    $header['total'] = $this->t('Total');


    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row = [];

    foreach (['label', 'key', 'test_types'] as $property) {
      $row[$property] = empty($entity->{$property}) ? '-' : implode(', ', (array) $entity->{$property});
    }

    return $row + parent::buildRow($entity);
  }

}
