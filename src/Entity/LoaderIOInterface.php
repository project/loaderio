<?php
/**
 * @file
 * LoaaderIO Test entity interface.
 */

namespace Drupal\loaderio\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

interface LoaderIOInterface extends ConfigEntityInterface {

  /**
   * Entity type.
   */
  const ENTITY_TYPE = 'testloaderio';

}
